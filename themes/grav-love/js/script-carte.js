var map = L.map('mapid').setView([50.8439, 4.3603], 14);
var layer = new L.StamenTileLayer("toner-lite"); // http://maps.stamen.com/#terrain/12/37.7706/-122.3782
map.addLayer(layer);

var formatPopup = function(properties) {
    var span = document.createElement('span');
    span.innerHTML =[
        '<h3><a href="' + properties.link + '">' + properties.name + '</a></h3>',
        properties.description
    ].join('<br/>');
    return span;
};

var xhr = new XMLHttpRequest();
xhr.open('GET', JSON_URI);
xhr.setRequestHeader('Content-Type', 'application/json');
xhr.responseType = 'json';
xhr.onload = function() {
    if (xhr.status !== 200) {
        console.log('request gone wrong!');
        return;
    };
    var geoJsonLayer = L.geoJSON(xhr.response, {
        onEachFeature: function (feature, layer) {
            layer.bindPopup(formatPopup(feature.properties));
        }
    });
    map.addLayer(geoJsonLayer);
};
xhr.send();
