---
title: 'Mothers and Daughters'
media_order: 910705_Disco.jpg
taxonomy:
    tag:
        - bar
        - tatoo
latitude: 50.8363
longitude: 4.352
---

Un bar,<br>
Un espace artistique,<br>
Un lieu lesbien et trans,<br>
Inclusif,<br>
Safer,<br>
Queer,<br>
Féministe.<br>
—Pensé par des lesbiennes & des trans<br>
pour les lesbiennes et leurs ami-e-s


[https://www.mothersanddaughters.be/](https://www.mothersanddaughters.be/)