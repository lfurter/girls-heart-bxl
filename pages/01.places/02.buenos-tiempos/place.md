---
title: 'Buenos Tiempos, Int.'
taxonomy:
    tag:
        - festival
        - bar
latitude: 50.8373
longitude: 4.352
---

Buenos Tiempos, Int. is an online exhibition space
thematically concerned with “faggotry as it is
today”, the programmer of an annual Evening of
Poetry is Brussels and a collaborative production
initiative focused on “today’s and yesterday’s transvestism” – these productions are presented with our
peers. Alberto García del Castillo and Marnie Slater
founded Buenos Tiempos, Int in 2014.