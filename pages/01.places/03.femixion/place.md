---
title: Femixion
taxonomy:
    tag:
        - festival
        - past
        - fanzine
latitude: 50.8501
longitude: 4.352
---

Femixion is a feminist science-fiction fanzine. For each issue we invite about
ten women whose work we love to contribute by offering photos, novels
and comics on a theme (already published : « Trou Noir »,
« Friction », « Fluides » and « Alcôve »).